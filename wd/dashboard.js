const primaryColor = '#4834d4'
const warningColor = '#f0932b'
const successColor = '#6ab04c'
const dangerColor = '#eb4d4b'


const body = document.getElementsByTagName('body')[0]


function collapseSidebar() {
	body.classList.toggle('sidebar-expand')
}

